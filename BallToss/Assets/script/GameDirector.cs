﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameDirector : MonoBehaviour
{
    public static int count = 0;
    public static float time = 1500.0f;
    public GameObject counter = null;
    GameObject timer;
    // Start is called before the first frame update
    void Start()
    {
        //ヒエラルキーからUIを呼ぶ
        this.counter = GameObject.Find("Counter");
        this.timer = GameObject.Find("timer");
    }

    public void ballcount()
    {
        //カウントを1増やす
        count++;
    }

    public void misscount()
    {
        count--;
    }

    // Update is called once per frame
    void Update()
    {
        //時間経過とともにtimeを減らす
        time -= Time.deltaTime;
        this.timer.GetComponent<Image>().fillAmount -= 0.00056f;

        //スコアを追加
        Text CounterText = this.counter.GetComponent<Text>();
        CounterText.text = count + "個目";
    }
}
