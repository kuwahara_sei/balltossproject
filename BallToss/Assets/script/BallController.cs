﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject Ball;
    public Rigidbody2D rb;
    float ballforce = 600.0f;

    // Start is called before the first frame update
    void Start()
    {
        //RigidBodyを呼ぶ
        rb = GetComponent<Rigidbody2D>();

        //ボールを飛ばす
        this.rb.AddForce(transform.up * this.ballforce);
        this.Ball = GameObject.Find("Ballprefab");
    }

    // Update is called once per frame
    void Update()
    {
        //y座標を超えたらボールを破棄、スコアも減らす
        if (transform.position.y < -10.0f) 
        {
            //GameObject director = GameObject.Find("GameDirector");
            //director.GetComponent<GameDirector>().misscount();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collison)
    {
        //スコア判定を追加
        GameObject director = GameObject.Find("GameDirector");
        director.GetComponent<GameDirector>().ballcount();

        //当たったら破棄する
        Destroy(gameObject);
        Debug.Log("あったった");
    }
}
