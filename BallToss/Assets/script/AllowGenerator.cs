﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllowGenerator : MonoBehaviour
{
    public GameObject AllowPrefab;

    // Start is called before the first frame update
    void Start()
    {
        GameObject go = Instantiate(AllowPrefab) as GameObject;
        int px = Random.Range(-3, 3);
        go.transform.position=new Vector3(px, 6, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
