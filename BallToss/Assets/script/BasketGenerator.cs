﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    float span = 1.5f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //時間経過した分deltaを増やす
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            //0に戻す
            this.delta = 0;

            //バスケットを出す
            GameObject ami = Instantiate(BasketPrefab) as GameObject;
            float Bas = Random.Range(-3.5f, -3.5f);
            ami.transform.position = new Vector3(-15, Bas, 0);
        }
    }
}
